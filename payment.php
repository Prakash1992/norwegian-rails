<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Norwegian Rails</title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=PT+Sans:400" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="css/bulma.min.css" />

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css" />

	<script type="text/javascript" src="cities.json"></script>

</head>

<body>
	<div id="booking" class="section">
		<div class="section-center">
			<div class="container">
				<div class="row">
					<div class="booking-form">
						<form action="success.php" method="post">
						<div class="panel panel-default">
  							<span class="glyphicon glyphicon-search"><div class="panel-heading">Order Your Payment</div></span>
						</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										
										<input class="form-control" type="text" placeholder="Name on Card">
									</div>
								</div>
							</div>	
							<div class="row">	
								<div class="col-md-6">
									<div class="form-group">
										<span class="glyphicon glyphicon-credit-card" aria-hidden="true"><input class="form-control" type="num" placeholder="Card Number"></span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<span class="form-label">CVC</span>
										<input class="form-control" placeholder="Ex. 353" type="number" required max="999">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<span class="form-label">Expiration</span>
										<input class="form-control" type="text" placeholder="MM" required>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
								<span class="form-label"></span>
										<input class="form-control" type="text" placeholder="YY" required>
									</div>
								</div>								


							</div>
							<div class="row">
								
									<div class="col-md-6">
									<input type="submit" class="btn btn-group-lg btn-success"  value="Pay Now">
									</div>
									<div class="col-md-6">
									<a  href= "http://localhost/booking" class="btn btn-group-lg btn-warning" >Change/Cancel Payment </a>
									</div>
									<!-- Modal -->
							
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>